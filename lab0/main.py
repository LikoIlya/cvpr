import random
import randomcolor

import cv2


def get_random_color():
    rand_color = randomcolor.RandomColor()
    color_str = rand_color.generate(format_='rgb')[0]
    rgb_list = [int(entry) for entry in color_str.replace('rgb(', '').replace(')', '').split(', ')]
    return tuple(rgb_list)


def main():
    # capture image from webcam
    video_capture = cv2.VideoCapture(0)
    if not video_capture.isOpened():
        raise Exception("Could not open video device")
    ret, image = video_capture.read()
    video_capture.release()

    # save captured image
    path = './image.png'
    cv2.imwrite(path,
                image)

    # load captured image in grayscale
    img_gray_mode = cv2.imread(path,
                               cv2.IMREAD_GRAYSCALE)

    # convert opened image from grayscale to RGB
    modified_img = cv2.cvtColor(img_gray_mode,
                                cv2.COLOR_GRAY2RGB)
    # get height and width of opened image
    height, width = modified_img.shape[:2]

    # draw line from upper left corner to down right corner on opened image with random thickness (from 1px to 10px)
    cv2.line(modified_img,
             (0, 0),
             (width, height),
             get_random_color(),
             random.randint(1, 10))

    # draw rectangle on random coords on opened image with random thickness (from 1px to 10px)
    cv2.rectangle(modified_img,
                  (random.randint(0, width), random.randint(0, height)),
                  (random.randint(0, width), random.randint(0, height)),
                  get_random_color(),
                  random.randint(1, 10))

    # save grayscale image
    cv2.imwrite('./gray.png', img_gray_mode)

    # save modified image
    cv2.imwrite('./modified_img.png', modified_img)

    # show saved image
    cv2.imshow("stock image", image)
    cv2.imshow("gray", img_gray_mode)
    cv2.imshow("modified img", modified_img)
    cv2.waitKey()


if __name__ == '__main__':
    main()
