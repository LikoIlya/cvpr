import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from tqdm import tqdm

i = 1

data_folder = os.path.join(os.curdir, 'img')
data_set = [os.path.join(data_folder, file)
            for file
            in os.listdir(data_folder)
            if os.path.isfile(os.path.join(data_folder, file))]

data_folder1 = os.path.join(os.curdir, 'img2')
data_set1 = [os.path.join(data_folder1, file)
             for file
             in os.listdir(data_folder1)
             if os.path.isfile(os.path.join(data_folder1, file))]

data_folder2 = os.path.join(os.curdir, 'img1')
data_set2 = [os.path.join(data_folder2, file)
             for file
             in os.listdir(data_folder2)
             if os.path.isfile(os.path.join(data_folder2, file))]


def manipulate_data_set(data_set, i):
    images = [cv2.imread(image, 0) for image in data_set]
    sns.set()
    # with sns.axes_style({'axes.grid': False}):
    # for idx, image in enumerate(images):
    #        plt.imshow(images[idx], 'gray'),plt.show()
    #        plt.figure(idx+1)

    key_pts1, key_pts2, good, matchesMask, msed = detect_match(algorithm=alg_dict['orb'], query_img=images[2],
                                                               train_img=images[0],
                                                               min_match_count=min_matches)
    print(msed)

    sns.set()
    with sns.axes_style({'axes.grid': False}):
        # if msed != np.inf:
        plot_match(query_img=images[2],
                   key_pts1=key_pts1,
                   train_img=images[0],
                   key_pts2=key_pts2,
                   good=good,
                   matchesMask=matchesMask)

    import itertools
    import time

    inliers_matches = dict()
    indexes = list(itertools.product(alg_dict.keys(), ["inliers/matches", "distance MSE", "time (ms)"]))
    for im_j_idx in range(len(images)):
        inliers_matches["img" + str(im_j_idx)] = list()
    for alg_name, alg in alg_dict.items():
        for im_i_idx, image_i in tqdm(enumerate(images)):
            # print("Comparing img{0} and img{1}".format(im_i_idx,im_j_idx))
            time_start = time.monotonic()
            _, __, match, inlier, msd = detect_match(algorithm=alg, query_img=image_i, train_img=images[22],
                                                     min_match_count=3)
            time_end = time.monotonic()
            if match == 0 or match is None:
                inliers_matches["img" + str(im_i_idx)].append(0)
            else:
                inliers_matches["img" + str(im_i_idx)].append(np.sum(inlier) / (len(match)))
            inliers_matches["img" + str(im_i_idx)].append(msd)
            inliers_matches["img" + str(im_i_idx)].append(time_end - time_start)

    import pandas as pd

    # pd.set_option('display.max_columns', 999)

    index = pd.MultiIndex.from_tuples(indexes, names=['algorithm', 'property'])
    comparison_df = pd.DataFrame(inliers_matches, index=index)
    output_file = "df_{}.xlsx".format(i)
    comparison_df.to_excel(output_file)
    print(comparison_df)


def detect_match(algorithm, query_img, train_img, min_match_count, verbose=False):
    query_image = query_img
    train_image = train_img
    for name, alg in alg_dict.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
        if alg == algorithm:
            if name == "brief":
                star = cv2.xfeatures2d.StarDetector_create()
                key_pts1 = star.detect(query_image, None)
                key_pts2 = star.detect(train_image, None)
                # compute the descriptors with BRIEF
                key_pts1, des1 = algorithm(query_image, key_pts1)
                key_pts2, des2 = algorithm(train_image, key_pts2)
            else:
                key_pts1, des1 = algorithm(query_image, None)
                key_pts2, des2 = algorithm(train_image, None)
    msed = np.inf
    if not (isinstance(des1, np.float32) & isinstance(des2, np.float32)):
        des1 = np.float32(des1)
        des2 = np.float32(des2)

    flann_idx = 1
    index_params = dict(algorithm=flann_idx, trees=5)
    search_params = dict(checks=50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    bf = cv2.BFMatcher()

    matches = flann.knnMatch(np.asarray(des1, np.float32), np.asarray(des2, np.float32), k=2)

    # store all the good matches as per Lowe's ratio test.
    good = [m for m, n in matches if m.distance < 0.7 * n.distance]

    if len(good) > min_match_count:
        src_pts = np.float32([key_pts1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dst_pts = np.float32([key_pts2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        if M is None:
            return None, None, None, None, np.inf
        # print(M,mask)
        matchesMask = mask.ravel().tolist()
        h, w = query_image.shape
        pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)
        msed = np.mean([np.sqrt(np.sum(diff)) for diff in (np.power(pts - dst, 2))] / (np.sqrt(h ** 2 + w ** 2)))
        # cv2.polylines(train_image,[np.int32(dst)],True,255,3, cv2.LINE_AA)
    else:
        if verbose == True:
            print("Not enough matches are found - {}/{}".format(len(good), min_match_count))
        matchesMask = None
        good = None

    return key_pts1, key_pts2, good, matchesMask, msed


def plot_match(query_img, train_img, key_pts1, key_pts2, good, matchesMask):
    draw_params = dict(matchColor=(255.0, 0, 0),  # draw matches in white color
                       singlePointColor=(255.0, 0, 0),
                       matchesMask=matchesMask,  # draw only inliers
                       flags=2)
    img3 = cv2.drawMatches(query_img, key_pts1, train_img, key_pts2, good, None, **draw_params)
    plt.imshow(img3, 'gray'), plt.show()


alg_dict = dict(surf=cv2.xfeatures2d.SURF_create(200).detectAndCompute,
                brief=cv2.xfeatures2d.BriefDescriptorExtractor_create().compute,
                orb=cv2.ORB_create().detectAndCompute)
min_matches = 4
manipulate_data_set(data_set, 1)
manipulate_data_set(data_set, 2)
manipulate_data_set(data_set2, 3)
