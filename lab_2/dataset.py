# -----------------------------------------
# DOWNLOAD AND ORGANIZE FLOWERS17 DATASET
# -----------------------------------------
import glob
import os
import tarfile
import urllib.request
from loguru import logger


def download_dataset(filename, url, work_dir):
    if not os.path.exists(filename):
        logger.info("Downloading flowers17 dataset....")
        filename, _ = urllib.request.urlretrieve(url + filename, filename)
        statinfo = os.stat(filename)
        logger.info("Succesfully downloaded " + filename + " " + str(statinfo.st_size) + " bytes.")
        untar(filename, work_dir)


def jpg_files(members):
    for tarinfo in members:
        if os.path.splitext(tarinfo.name)[1] == ".jpg":
            yield tarinfo


def untar(fname, path):
    tar = tarfile.open(fname)
    tar.extractall(path=path, members=jpg_files(tar))
    tar.close()
    logger.info("Dataset extracted successfully.")


def organize_dataset():
    flowers17_url = "http://www.robots.ox.ac.uk/~vgg/data/flowers/17/"
    flowers17_name = "17flowers.tgz"
    train_dir = os.path.join(os.getcwd(), "dataset")

    if not os.path.exists(train_dir):
        os.makedirs(train_dir)

    download_dataset(flowers17_name, flowers17_url, train_dir)
    if os.path.exists(os.path.join(train_dir, "jpg")):
        os.rename(os.path.join(train_dir, "jpg"), os.path.join(train_dir, "train"))

    # get the class label limit
    class_limit = 17

    # take all the images from the dataset
    image_paths = glob.glob(os.path.join(train_dir, "train") + "/*.jpg")

    # variables to keep track
    label = 0
    i = 0
    j = 80

    # flower17 class names
    class_names = ["daffodil", "snowdrop", "lilyvalley", "bluebell", "crocus",
                   "iris", "tigerlily", "tulip", "fritillary", "sunflower",
                   "daisy", "coltsfoot", "dandelion", "cowslip", "buttercup",
                   "windflower", "pansy"]

    # loop over the class labels
    for x in range(1, class_limit + 1):
        # create a folder for that class
        cur_path = os.path.join(train_dir, "train", class_names[label])
        if not os.path.exists(cur_path):
            os.makedirs(cur_path)

        # loop over the images in the dataset
        for index, image_path in enumerate(image_paths[i:j], start=1):
            original_path = image_path
            image_path = image_path.split("/")
            image_file_name = str(index) + ".jpg"
            os.rename(original_path, os.path.join(cur_path, image_file_name))

        i += 80
        j += 80
        label += 1


# -------------------------
# MAIN FUNCTION
# -------------------------
if __name__ == '__main__':
    organize_dataset()
