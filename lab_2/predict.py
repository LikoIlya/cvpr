# -----------------------------------
# TRAINING OUR MODEL
# -----------------------------------
import glob
import os
import warnings

import cv2
import h5py
import numpy as np
from matplotlib import pyplot
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from loguru import logger
from lab_2.dataset import organize_dataset
from lab_2.train_features import fixed_size, fd_hu_moments, fd_haralick, fd_histogram, train

warnings.filterwarnings('ignore')

# --------------------
# tunable-parameters
# --------------------
num_trees = 100
test_size = 0.10
seed = 9
train_path = os.path.join(os.getcwd(), "dataset", "train")
test_path = os.path.join(os.getcwd(), "dataset", "test")
h5_data = os.path.join(os.getcwd(), "output", "data.h5")
h5_labels = os.path.join(os.getcwd(), "output", "labels.h5")
scoring = "accuracy"

organize_dataset()
train()
# get the training labels
train_labels = os.listdir(train_path)

# sort the training labels
train_labels.sort()

if not os.path.exists(test_path):
    os.makedirs(test_path)

# create all the machine learning models
models = [
    ('LR', LogisticRegression(random_state=seed)),
    ('LDA', LinearDiscriminantAnalysis()),
    ('KNN', KNeighborsClassifier()),
    ('CART', DecisionTreeClassifier(random_state=seed)),
    ('RF', RandomForestClassifier(n_estimators=num_trees, random_state=seed)),
    ('NB', GaussianNB()),
    ('SVM', SVC(random_state=seed))
]

# variables to hold the results and names
results = []
names = []

# import the feature vector and trained labels
h5f_data = h5py.File(h5_data, 'r')
h5f_label = h5py.File(h5_labels, 'r')

global_features_string = h5f_data['dataset_1']
global_labels_string = h5f_label['dataset_1']

global_features = np.array(global_features_string)
global_labels = np.array(global_labels_string)

h5f_data.close()
h5f_label.close()

# verify the shape of the feature vector and labels
logger.info("Features shape: {}".format(global_features.shape))
logger.info("Labels shape: {}".format(global_labels.shape))

logger.info("Training started...")

# split the training and testing data
(trainDataGlobal, testDataGlobal, trainLabelsGlobal, testLabelsGlobal) = train_test_split(np.array(global_features),
                                                                                          np.array(global_labels),
                                                                                          test_size=test_size,
                                                                                          random_state=seed)

logger.success("Splitted train and test data...")
logger.info("Train data  : {}".format(trainDataGlobal.shape))
logger.info("Test data   : {}".format(testDataGlobal.shape))
logger.info("Train labels: {}".format(trainLabelsGlobal.shape))
logger.info("Test labels : {}".format(testLabelsGlobal.shape))

# 10-fold cross validation
for name, model in models:
    kfold = KFold(n_splits=10, random_state=seed)
    cv_results = cross_val_score(model, trainDataGlobal, trainLabelsGlobal, cv=kfold, scoring=scoring)
    results.append([x*10 for x in cv_results])
    names.append(name)
    msg = "%s: Accuracy: %0.2f (+/- %0.2f)" % (name, cv_results.mean()*10, cv_results.std()*20)
    logger.success(msg)

# boxplot algorithm comparison
fig = pyplot.figure()
fig.suptitle('Machine Learning algorithm comparison')
ax = fig.add_subplot(111)
pyplot.boxplot(results)
ax.set_xticklabels(names)
pyplot.show()

# -----------------------------------
# TESTING OUR MODEL
# -----------------------------------
# create the model - KNN
clf = KNeighborsClassifier()

# fit the training data to the model
clf.fit(trainDataGlobal, trainLabelsGlobal)

# loop through the test images
for file in glob.glob(test_path + "/*.jpg"):
    # read the image
    image = cv2.imread(file)

    # resize the image
    image = cv2.resize(image, fixed_size)

    ####################################
    # Global Feature extraction
    ####################################
    fv_hu_moments = fd_hu_moments(image)
    fv_haralick = fd_haralick(image)
    fv_histogram = fd_histogram(image)

    ###################################
    # Concatenate global features
    ###################################
    global_feature = np.hstack([fv_histogram, fv_haralick, fv_hu_moments])
    # predict label of test image
    prediction = clf.predict(global_feature.reshape(1,-1))[0]

    # show predicted label on image
    cv2.putText(image, train_labels[prediction], (20, 30), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 255), 3)

    # display the output image
    pyplot.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    pyplot.show()
